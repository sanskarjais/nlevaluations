from django.contrib import admin
from .models import App, Task, UserDownloads

#Models are registered here for the admin panel
admin.site.register(App)
admin.site.register(Task)
admin.site.register(UserDownloads)
