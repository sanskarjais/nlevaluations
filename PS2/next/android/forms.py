from django import forms
from .models import Task, App
from .category import Choice

#Form for verifying the app downlaod
class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ('screenshot',)

#Form for adding the new app by the admin
class AppForm(forms.ModelForm):

    # app_category = forms.ChoiceField(choices = Choice,  widget=forms.Select(),required = True)
    # sub_category = forms.ChoiceField(choices = Choice, widget=forms.Select(), required = True)
    
    class Meta:
        model = App
        fields = ('name', 'applink','points','img','app_category','sub_category',)
        widgets = {
            'app_category' : forms.Select(choices = Choice),
            'sub_category' : forms.Select(choices = Choice),
        }