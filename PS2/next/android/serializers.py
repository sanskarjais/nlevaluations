from rest_framework import serializers

from .models import App

#Serializer which generates the data as json format which can be used further 
class AppSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = App
        fields = ['name','applink','app_category','sub_category','points']