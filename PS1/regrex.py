#Importing all the packages regrex to handle the regular expression and json for handling the dictionary
import re
import json

#Empty list to store the numbers
numbers = []

#Given expression
l = {"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

#Converting the expression into string format
l = json.dumps(l)

#Regular expression for handling the string it contais everything except numbers
pattern = re.compile(r'"id":([^,]+)}')

#Processing the string and converting into numbers and finally store into the list
for id in re.finditer(pattern, l): 
  numbers.append(int(id.group(1)))

#Final list contains all the numbers
print(numbers)

