Problem set 1
Open the regrex.py, placed file inside the folder named "PS1" it gives you all the numbers inside the expression.

Problem set 2
https://whispering-sands-75318.herokuapp.com/ 
Create a virtual env in the same folder "PS2" by "python -m venv env" and activate the environment by "env\script\activate"
Open the folder "PS2" and install requirements.txt by initiating the command "pip install requirements.txt"
on the prompt run python manage.py runserver and login as admin with the credentials username : "admin" pass : "admin"

For accessing the api go to the url 'api/apps' to get the API for the applications.

Problem Set 3
A ->

Flask framework is simple and more flexible and suitable for single application while Djnago 
allows to have multiple page application within a single project we can have different apps
enrolled and also its a Full Stack Web framework.
When we want to make single application go for flask and rember it does not supports third
party application annd no inbuilt database.
Django allows large number of third party applications and suitable for large scale projects.


B->
I’m going to select Pfair scheduling. It was proposed as a way of optimally & efficiently 
scheduling periodic tasks on a multiprocessor system. It is different from more conventional 
time scheduling disciplines. It ensures uniform execution rates by breaking jobs into smaller
sub-jobs.  Pfair scheduling tracks the allocation of processor time
in a fluid schedule.
Since it is uniformly divides the jobs in sub-jobs it makes the overall CPU load less and
form an uniform window for each sub-tasks that makes it more productive. It ensures that 
the scalability of the  system is high. Due to its uniform breaking of each job in sub jobs 
makes it tend to get preempted after each of their constituent sub-jobs complete execution. 
Thus pfair schedules sometimes contain a large number of job preemptions and context-switches.
